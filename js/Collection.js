var collection = (function () {
var tasks= Storage.Get_tasks();
var	index= 0;
var	count=Storage.Get_count();
var	distanceInWordsToNow= dateFns.distanceInWordsToNow;
function getIndex(id) {
		for (var i = 0; i < tasks.length; i++) {
			if (tasks[i].id == id) {
				return i;
			}
		}
		return -1;
};
View.render(tasks);
View.set_count(count);
return  {		

	add(task) {
		document.querySelector('.toggle-all').checked=false;		
		task.id = ++index;
		tasks.push(task);
		View.render(tasks);		
		View.set_count(++count);
		Storage.Save(tasks,count);
	},

	remove(id) {
		let index=getIndex(id);
		if(index>=0){
		let t=tasks[index].completed;			
		tasks.splice(index, 1);
		View.render( tasks);
			if(!t)
				View.set_count(--count);
			else
				View.set_count(count);
		Storage.Save(tasks,count);		
		}	
	},

	change(id,text) {
		tasks[getIndex(id)].editing = !tasks[getIndex(id)].editing;
		if(!tasks[getIndex(id)].editing)
			tasks[getIndex(id)].text=text;
		View.render(tasks);
		Storage.Save(tasks,count);		
	},


	complete(id) {
		document.querySelector('.toggle-all').checked=false;	
		tasks[getIndex(id)].finished=distanceInWordsToNow(
			tasks[getIndex(id)].created,
			{includeSeconds: true});
		tasks[getIndex(id)].completed = !tasks[getIndex(id)].completed;
		View.render(tasks);		
		if(tasks[getIndex(id)].completed)
		View.set_count(--count);
		else
		View.set_count(++count);
		Storage.Save(tasks,count);

	},

	

	
}


})();