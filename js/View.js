var View = (function () {

function countCompleted(count)
	{
	let countAll=document.querySelector('.todo-list').childElementCount;
    return countAll-count;
	};
function StyleSection(style)
	{
	document.querySelector('.footer').style=style;
	document.querySelector('.main').style=style;
	};

var parent=document.querySelector('.todo-list');


return {
	set_count(count){
	document.querySelector('.todo-count strong').textContent=count;
	if(count==0 && countCompleted(count)==0)
			StyleSection('display:none;');
		else
			StyleSection('');
	if(countCompleted(count)==0)
    	document.querySelector('.clear-completed').style='display:none;';
    else
    	document.querySelector('.clear-completed').style='';
	},
	

	render(tasks) {
		parent.innerHTML = tasks.reduce((str, task) => 
			`<li data-id="${task.id}" class="${task.completed ? 'completed' : ''} ${task.editing ? 'editing' : ''}">
				<div class="view">
					<input class="toggle" 
						type="checkbox" 
						${task.completed ? 'checked' : ''}
						onclick="onComplete(${task.id})">
					<label ondblclick="onEditing(${task.id})" style="display:inline-block;max-width:280px;">${task.text}</label>
					<div class="date_finished">${task.completed ? task.finished : ''}</div>
					<button class="destroy" onclick="onRemove(${task.id})"></button>
				</div>
				<input class="edit" value="${task.text}">
			</li>${str}`
		, '');
	},
	Routing(ul,filter){
	switch (filter)
	{
	case 'All':
	for (var i = 0; i < ul.children.length; i++) {
		ul.children[i].style='';
	}
	break;
	case 'Active':
	for (var i = 0; i < ul.children.length; i++) {
		if(ul.children[i].matches(".completed"))
		ul.children[i].style='display:none;';
		else
		ul.children[i].style='';
	}
	break;
	case 'Completed':
	for (var i = 0; i < ul.children.length; i++) {
		if(!ul.children[i].matches(".completed"))
		ul.children[i].style='display:none;';
		else
		ul.children[i].style='';
	}
	break;
	}
}

}
})();