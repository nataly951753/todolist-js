(function (window) {
	'use strict';

const element=document.querySelector('.todoapp');
const ul=document.querySelector('.todo-list');
const ulFilters=document.querySelector('.filters');
let filter='All';

element.addEventListener('keypress', function (e) {	
if(e.which==13 && e.target.value!='')
{	
		if(e.target.matches('.new-todo'))
		{
			const task = new Task(e.target.value);
			collection.add(task);
			e.target.value = "";
			View.Routing(ul,filter);
		}
		else if(e.target.matches('.edit'))
		{		
		let elementLi=e.target.parentElement;
		collection.change(elementLi.dataset['id'],e.target.value);			
		}
}});



element.addEventListener('click', function (e) {
	View.Routing(ul,filter);	
	if(e.target.matches('.toggle-all')){
		if(e.target.checked)
		{
			for (var i = 0; i < ul.children.length; i++) {
				if(!ul.children[i].matches('.completed'))
				collection.complete(ul.children[i].dataset['id']);
			}
			document.querySelector('.toggle-all').checked=true;
		}
		else
		{
			for (var i = 0; i < ul.children.length; i++) {
				if(ul.children[i].matches('.completed'))
				collection.complete(ul.children[i].dataset['id']);
			}
		}
	}
	else if(e.target.matches('.clear-completed')){
		for (var i = 0; i < ul.children.length; ) {
				if(ul.children[i].matches('.completed'))
				collection.remove(ul.children[i].dataset['id']);
				else
					i++;
			}
	}
	else if(e.target.matches('.filters li a'))
	{
		for (var i = 0; i < ulFilters.children.length; i++) {
			ulFilters.children[i].children[0].classList.remove('selected');
		}
		e.target.classList.add('selected');
		filter=e.target.textContent;
		View.Routing(ul,filter);
	}
}
);
})(window);


function onRemove(id) {
	collection.remove(id);
}
function onComplete(id) {
	collection.complete(id);
}
function onEditing(id){
	collection.change(id);
}