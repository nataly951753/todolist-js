var Storage = (function () {

return {
	Save(tasks,count){
		localStorage.setItem('tasks_for_todos', JSON.stringify(tasks));
		localStorage.setItem('count_of_tasks_for_todos', JSON.stringify(count));

	},
	Get_tasks(){
		if(localStorage['tasks_for_todos']!== undefined)
			return JSON.parse(localStorage['tasks_for_todos']);
		else
			return [];
	},
	Get_count(){
		if(localStorage['count_of_tasks_for_todos']!== undefined)
			return JSON.parse(localStorage['count_of_tasks_for_todos']);
		else
			return 0;
	}
}
})();