function Task(text) {
	this.text = text;
	this.completed = false;
	this.editing = false;
	this.created = new Date();
	this.finished=null;
	this.id = null;
}